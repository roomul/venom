#!/bin/bash
#

REPOS="main multilib nonfree user"
REPO_DIR="/usr/ports"
REPO_D="$PWD/ports"

[[ -d "$REPO_D" ]] || install -dm755 $REPO_D

for repo in $REPOS ; do
	if [ -d "$REPO_D/$repo" ]; then
		printf "Copy $repo\n"
		cp -Rf $REPO_DIR/$repo $REPO_D
	fi
done

#[[ -f "$REPO_D/version" ]] || exit 1
#VERSION=$(cat /$REPO_D/version)
#[[ -f "ports-$VERSION.tar.xz" ]] && rm -v ports-$VERSION.tar.xz

#printf "Pack ports-$VERSION.tar.xz\n"
#tar -cpf - ports | xz -9 -c - > ports-$VERSION.tar.xz
printf "Done.\n"
sync
exit 0
